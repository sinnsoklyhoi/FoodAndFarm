//
//  ShowMapViewController.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

class ShowMapViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var centerMapCoordinate:CLLocationCoordinate2D!
    var buttonLocation : UIButton!
    var buttonPickUp : UIButton!
    var marker = GMSMarker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.isMyLocationEnabled = true
        self.mapView.userActivity?.delegate = self as? NSUserActivityDelegate
        self.mapView.delegate = self
        self.mapView.selectedMarker = marker
        self.mapView.settings.myLocationButton = true
        
        // User Location
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //Call Fuction
        createButtonForClickLocation()
        // Hidden Button Pick UP
        
    }
    
   /* func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")

    } */
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
     
        print("Error \(error)")
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let lat = mapView.camera.target.latitude
        let long = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: lat , longitude: long)
        self.placeMarkerOnCenter(position: centerMapCoordinate)
        
        print(lat)
        print(long)
    }
    
    func placeMarkerOnCenter(position:CLLocationCoordinate2D) {
        //Create Marker when click
    }
    
    func createButtonForClickLocation()  {
        
        buttonLocation = UIButton(frame: CGRect(x : CGFloat(100), y: CGFloat(100), width: CGFloat(70), height: CGFloat(20)))
        buttonLocation.setTitle("Pick UP", for: .normal)
        buttonLocation.backgroundColor = .red
        buttonLocation.layer.cornerRadius = 5
        buttonLocation.center = self.view.center
        buttonLocation.layer.zPosition = 2
        buttonLocation.addTarget(self, action: #selector(buttonPickUPByUser), for: .touchUpInside)
        self.view.addSubview(buttonLocation)
    }
    
    @objc func buttonPickUPByUser() {
        
        self.buttonLocation.isHidden = false
        marker.position = centerMapCoordinate
        marker.map = self.mapView
        
    }
    
}

