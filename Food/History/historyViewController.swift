//
//  historyViewController.swift
//  Food
//
//  Created by GIS on 8/2/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class historyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var getDate = [String]()
    
    @IBOutlet weak var tbVC: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
    
        
        
        tbVC.delegate = self
        tbVC.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getDate.count
        print("Data",getDate.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "History", for: indexPath) as! historyOrderTableViewCell
        
        cell.foodImage.image = UIImage(named: "image3")
        cell.orderLocation.text = getDate[indexPath.row]
        cell.dateOrder.text = getDate[indexPath.row]
        cell.timeOrder.text = getDate[indexPath.row]
        
        return cell
    }
}
