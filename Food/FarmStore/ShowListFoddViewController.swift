//
//  ShowListFoddViewController.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class ShowListFoddViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var tbVC: UITableView!
    
    var Foods = ["A","B","C","D","E","F"]
    var location = ["Phnom Penh","Kompong Cham","Takaev","Seama Reab","Kror Jes","Ta Kmov"]
    var imageFoods = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5"),#imageLiteral(resourceName: "image4")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tbVC.dataSource = self
        tbVC.delegate = self
        
     
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Foods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "FarmStore", for: indexPath) as! foodItemTableViewCell
        
        cell.farmPlace.text = Foods[indexPath.row]
        cell.location.text = location[indexPath.row]
        cell.imageFood.image = imageFoods[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let seletedCell = tableView.cellForRow(at: indexPath)
        performSegue(withIdentifier: "Detail", sender: seletedCell)
    }

    @IBAction func backToRestuarentsButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func orderButton(_ sender: Any) {
        
        performSegue(withIdentifier: "SingIn", sender: nil)
    }
    
}








