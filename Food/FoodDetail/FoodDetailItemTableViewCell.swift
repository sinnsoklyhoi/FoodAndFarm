//
//  FoodDetailItemTableViewCell.swift
//  Food
//
//  Created by GIS on 7/25/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class FoodDetailItemTableViewCell: UITableViewCell {

    @IBOutlet weak var imageFood: UIImageView!
    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var disPrice: UILabel!
    @IBOutlet weak var Quality: UILabel!
    @IBOutlet weak var minQuality: UIButton!
    @IBOutlet weak var addQuality: UIButton!
    
}
//NSStrikethroughStyleAttributeName
