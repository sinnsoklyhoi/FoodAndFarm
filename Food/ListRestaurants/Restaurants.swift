//
//  Restaurants.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import Foundation

struct Restaurant : Decodable {
    
    let resName  :  String!
    let location : String!
    let resImage : String!
}
