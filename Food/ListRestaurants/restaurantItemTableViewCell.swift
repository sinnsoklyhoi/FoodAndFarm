//
//  restaurantItemTableViewCell.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class restaurantItemTableViewCell: UITableViewCell {

    @IBOutlet weak var imageUrl: UIImageView!
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var resLocation: UILabel!

}
