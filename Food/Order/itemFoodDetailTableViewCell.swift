//
//  itemFoodDetailTableViewCell.swift
//  Food
//
//  Created by GIS on 8/2/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class itemFoodDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var sizeFood: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var quality: UILabel!
    @IBOutlet weak var viewFoodDetail: UIView!
    
}
