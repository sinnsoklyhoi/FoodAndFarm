//
//  orderFoodViewController.swift
//  Food
//
//  Created by GIS on 7/27/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class orderFoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tbVC: UITableView!
    
    var food = ["1"]
    var items = 1
    var a = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbVC.dataSource = self
        tbVC.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return food.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderFood", for: indexPath) as! orderItemTableViewCell
        cell.foodName.text = "A"
        cell.quality.text = "1L"
        cell.price.text = "1$"
        cell.priceOfQuality.text = "2"
        cell.Qualitys.text = String(items)

        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            food.remove(at: indexPath.row)
            tbVC.beginUpdates()
            tbVC.deleteRows(at: [indexPath], with: .automatic)
            tbVC.endUpdates()
            print(food)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    @IBAction func addMoreFood(_ sender: Any) {
        
        food.append("Food\(food.count + 1)")
        let  insertPath = NSIndexPath(item: food.count - 1 , section: 0)
        tbVC.insertRows(at: [insertPath as IndexPath], with: .automatic)
        print("tag",(sender as AnyObject).tag)
    }
    
    @IBAction func addQualityButton(_ sender: Any) {

        items += 1
        tbVC.reloadData()
    }
    
    @IBAction func minQualityButton(_ sender: Any) {
        
        items -= 1
        if items == 0 {
            
            items = 1
            tbVC.reloadData()
        }
        tbVC.reloadData()
    }
    
    @IBAction func addToCardButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        //Sand Notification
        NotificationCenter.default.post(name: Notification.Name("show"), object: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backToFoodDetailButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
}











